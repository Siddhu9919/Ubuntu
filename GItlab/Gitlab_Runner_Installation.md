# Install GitLab Runner

# Download the binary for your system
````
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
````

# Give it permission to execute
````
sudo chmod +x /usr/local/bin/gitlab-runner
````

# Create a GitLab Runner user
````
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
````
# Install and run as a service
````
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
````
````
sudo gitlab-runner start
````
````
sudo gitlab-runner status
````
# register a gitlab-runner change with your token
````
gitlab-runner register  --url https://gitlab.com  --token glrt-eUxUyrD-wqyvx4HdkXyL
````
````
[https://gitlab.com]: Enter
````
````
[ip-172-31-3-100]: Enter
````
# Enter an executor: docker, docker-windows, shell, ssh, virtualbox, instance, kubernetes, custom, parallels, docker-autoscaler, docker+machine:
````
docker
````
# Enter the default Docker image (for example, ruby:2.7):
````
nareshbabu1991/petclinic:latest
````
